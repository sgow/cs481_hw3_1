﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_hw3_1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]


    public partial class NavPage : ContentPage
    {
       
        public NavPage()
        {
            

            InitializeComponent();
        }


        

        async void First_page(object sender, System.EventArgs e)
        {

            await Navigation.PushAsync(new Page1());
        }
        async void Second_page(object sender, System.EventArgs e)
        {

            await Navigation.PushAsync(new Page2());
        }
        async void Third_page(object sender, System.EventArgs e)
        {

            await Navigation.PushAsync(new Page3(null));
        }

    



        //data persistance reference https://codemilltech.com/persist-whatever-you-want-with-xamarin-forms/
        //object ->json serialization ->string -> property sotrage
    }
}